# TRANSLATES

To generate file '**template.pot**', did use terminal command:

```bash
cd minertrade
xgettext -n *.lua -L Lua --force-po --keyword=portableboxes.translate --from-code=UTF-8 -o ./locale/template.pot

```

To translate '**template.pot**' to your language use app poedit.:


### Cria arquivo .po a partir de um arquivo de templante .pot

Sintaxe:

```bash
$ msginit --no-translator --no-wrap --locale=$LANG.UTF-8 --output-file=$LANG.po --input=$POT
```

### Atualiza arquivo ,po a partir de um arquivo de templante .pot

Sintaxe:
```bash
$ msgmerge --sort-output --no-wrap --update --backup=off $LANG.po $POT
```

Example:
```bash
msgmerge --sort-output --no-wrap --update --backup=off ./locale/pt_BR.po ./locale/template.pot
```

### Install the PoEdit:

```bash
sudo apt-get install poedit
```

* Locales used: ca;cs;da;de;dv;eo;es;et;fr;hu;id;it;ja;jbo;kn;lt;ms;nb;nl;pl;pt;pt_BR;ro;ru;sl;sr_Cyrl;sv;sw;tr;uk


### Exemple of enable the brazilian portuguese language in minetest:

Translate Sample: 

* "locale/pt_BR.po" to brazilian portuguese language.

To enable the translate to brazilian portuguese language, write "language = pt_BR" in file "minetest.conf". Or write the command ```/set -n language pt_BR``` in game chat, and run again the minetest game.

# Convert '.po' file to '.tr' file.

### COMMAND SAMPLE: TRANSLATE TO BRASILIAN PORTUGUESSE
````bash
$ cd ./locale/
$ lua5.3 po2tr.lua "portableboxes" "pt_BR.po"
$ mv "pt_BR.tr" "portableboxes.pt_BR.tr"
$ cat portableboxes.pt_BR.tr | less
````

### PLEASE SUBMIT YOUR NEW TRANSLATION TO THE DEVELOPERS OF THIS MOD THROUGH THE GIT PROJECT BELOW:

https://gitlab.com/lunovox/portable-boxes

----

> See more: 
* https://forum.minetest.net/viewtopic.php?f=47&t=21974
* https://github.com/minetest/minetest/issues/8158
* https://gist.githubusercontent.com/mamchenkov/3690981/raw/8ebd48c2af20c893c164e8d5245d9450ad682104/update_translations.sh
* https://gitlab.com/4w/xtend/-/blob/master/xtend_default/tools/convert_po_file_to_tr_file/convert_po_file_to_tr_file.lua
