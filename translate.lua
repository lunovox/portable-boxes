local ngettext

if minetest.get_modpath("intllib") then
	if intllib.make_gettext_pair then
		-- New method using gettext.
		portableboxes.translate, ngettext = intllib.make_gettext_pair()
	else
		-- Old method using text files.
		portableboxes.translate = intllib.Getter()
	end
	minetest.log("action", "[PORTABLEBOXES] Tradutor 'intllib'!")
elseif minetest.get_translator ~= nil and minetest.get_current_modname ~= nil and minetest.get_modpath(minetest.get_current_modname()) then
	portableboxes.translate = minetest.get_translator(minetest.get_current_modname())
	minetest.log("action", "[PORTABLEBOXES] Tradutor padrão!")
else
	portableboxes.translate = function(s) return s end
	minetest.log("warning", "[PORTABLEBOXES] Sem Tradutor!")
end
