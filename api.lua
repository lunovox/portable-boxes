

portableboxes.canIntect=function(pos, playername)
	local meta = minetest.get_meta(pos)
	if 
		meta:get_string("owner")==playername 
		or (
			minetest.get_modpath("tradelands") 
			and modTradeLands.getOwnerName(pos)~="" 
			and modTradeLands.canInteract(pos, playername)
		) or (
			minetest.get_modpath("areas") 
			and #areas:getNodeOwners(pos)>=1 
			and areas:canInteract(pos, playername)
		)
	then
		return true
	end
	return false
end

portableboxes.register_box = function(objname, def)
	
	local setMetaDefault = function(pos, user, namebox)
		local meta = minetest.get_meta(pos)
		local ownername = user:get_player_name()
		
		
		meta:set_string("owner", ownername)
		meta:set_int("state", 0)
		meta:get_inventory():set_size("inbox", 
			def.slots.width * def.slots.height
		)
		--meta:get_inventory():set_size("main", 32)
		
		if namebox==nil or type(namebox)~= "string" or namebox=="" then 
			namebox = def.label_default 
		end
		meta:set_string("namebox", namebox:trim())
	
		if namebox:trim() ~= "" then
			meta:set_string("infotext", 
				core.colorize("#FFFF00", namebox:trim()).."\n"
				..core.colorize("#CCCCCC", portableboxes.translate("by: @1", ownername))
			)
		else
			meta:set_string("infotext", 
				core.colorize("#FFFF00", def.label_default).."\n"
				..core.colorize("#CCCCCC", portableboxes.translate("by: @1", ownername))
			)
		end
		
		meta:set_string("formspec",
		"size[5.9,8.4]" 
		.. default.gui_bg
		.. default.gui_bg_img
		--.."background[-0.25,-0.25;6.40,8.30;tex_woodbox_256x256.png]"
		.."image["..(((8 - def.slots.width)/2)*0.7 -0.2)..",1.45;"..def.slots.width..","..def.slots.height..";tex_woodbox_256x256.png]"
		
		.."style_type[list;size=0.8;spacing=0.1]"
		.."style_type[field;size=0.8;spacing=0.1]"
		.."field[0.35,0.7;5.2,0.8;namebox;"
			..minetest.formspec_escape(portableboxes.translate("Box Name")..":")
			..";"..minetest.formspec_escape(namebox)
		.."]"
		
		.."tooltip[btnSaveText;"..minetest.formspec_escape(portableboxes.translate("SAVE BUTTON"))..";#CCCC0088;#000000]"
		--.."style_type[button;bgimg=sbl_save.png;bgimg_pressed=sbl_save_pressed.png;border=false]"
		.."style[btnSaveText;bgimg=sbl_save.png;bgimg_hovered=sbl_save_pressed.png;bgimg_pressed=sbl_save_pressed.png;border=false]"
		.."image_button_exit[5.2,0.4;0.8,0.8;sbl_save.png;btnSaveText;]"
		
	
		.."list[current_player;main;0,5.4;8,4;]" 
		--.."tooltip[main;"..minetest.formspec_escape(portableboxes.translate("SAVE BUTTON"))..";#CCCC0088;#000000]"
		--listcolors[slot_bg_normal;slot_bg_hover;slot_border;tooltip_bgcolor;tooltip_fontcolor]
		.."listcolors[#666666CC;#CCCCCC88;#000000CC;#00000044;#FFFFFF]"
		.."box[-0.17,5.15;6,3.5;#111111]"
		.."list[context;inbox;"..(((8 - def.slots.width)/2)*0.7)..",1.7;"..def.slots.width..","..def.slots.height..";]"	
		.."listring[current_player;main]" 
		.."listring[current_name;inbox]"
		)
		
	
		
	end
	
	local newDef = {
		description = core.colorize("#FFFF00", def.title),
		drawtype = "nodebox",
		drop=objname,
		paramtype = "light", --Nao sei pq, mas o blco nao aceita a luz se nao tiver esta propriedade
		paramtype2 = "facedir", 
		tiles = def.tiles,
		node_box = def.box_format,
		selection_box = def.box_format,
	
		after_place_node = function(pos, placer, itemstack)
			local p1 = pos
			local namebox = ""
			
			local myDir = minetest.dir_to_facedir(placer:get_look_dir())
			local item=itemstack:to_table()
	
			minetest.set_node(p1, {name = objname, param1 = "", param2 = myDir})
				
			minetest.sound_play(def.sounds.place, {pos=p1, gain = 1.0, max_hear_distance = 5})
	
			if not (item.meta or item.metadata) then
				itemstack:take_item()
				return itemstack
			end
	
			if item.meta.items then
				local its = minetest.deserialize(item.meta.items or "") or {}
				local items = {}
				for i,it in pairs(its) do
					table.insert(items,ItemStack(it))
				end
				minetest.get_meta(p1):get_inventory():set_list("inbox",items)
			elseif item.metadata ~= "" then
				local meta=minetest.deserialize(item["metadata"])
				local s=meta.stuff
				local its=meta.stuff.split(meta.stuff,",",",")
				local nmeta=minetest.get_meta(p1)
				for i,it in pairs(its) do
					if its~="" then
						nmeta:get_inventory():set_stack("inbox",i, ItemStack(it))
					end
				end
			end
			
			if item.meta.namebox then
				namebox = item.meta.namebox
			end
	
			setMetaDefault(p1,placer, namebox)
		
			itemstack:take_item()
			return itemstack
		end,
		sounds = def.sounds.default,
		groups = def.groups,
		allow_metadata_inventory_put = function(pos, listname, index, stack, player)
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			local playername = player:get_player_name()
			if portableboxes.canIntect(pos, playername) then
				if stack:get_name() == objname then
					minetest.chat_send_player(playername, portableboxes.translate("There is not enough space to fit this item!"))
					return 0
				elseif not inv:room_for_item("inbox",stack) then
					minetest.chat_send_player(playername, portableboxes.translate("Full"))
					return 0
				end
				return stack:get_count()
			end
			return 0
		end,
	
		allow_metadata_inventory_take = function(pos, listname, index, stack, player)
			local playername = player:get_player_name()
			local meta = minetest.get_meta(pos)
			if not portableboxes.canIntect(pos, playername) then return 0 end
			return stack:get_count()
		end,
	
		allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)
			local playername = player:get_player_name()
			local meta = minetest.get_meta(pos)
			if not portableboxes.canIntect(pos, playername) then return 0 end
			return count
		end,
	
		can_dig = function(pos, player)
			local isCanDig = false
			if type(player)~="nil" and minetest.is_player(player) then
				local pinv = player:get_inventory()
				if type(pinv)~="nil" then
					local box = ItemStack(objname):to_table()
					box.meta={emptybox = os.date("%Y-%B-%d %Hh:%Mm:%Ss")}
					if pinv:room_for_item("main",ItemStack(box)) then
						local playername = player:get_player_name()
						local meta = minetest.get_meta(pos)
						isCanDig = portableboxes.canIntect(pos, playername)
					end
				end
			end
			return isCanDig
		end,
	
		on_punch = function(pos, node, player, pointed_thing)
			local meta=minetest.get_meta(pos)
			local ownername = meta:get_string("owner")
			local newNameBox = meta:get_string("namebox")
			local newDescription = ""
			local playername = player:get_player_name()
			
			if newNameBox ~= nil and type(newNameBox) == "string" and newNameBox~="" then
				newDescription = core.colorize("#FFFF00", newNameBox:trim())
				.."\n"..core.colorize("#CCCCCC", portableboxes.translate("by: @1", ownername))
			else
				newNameBox = def.label_default
				newDescription = core.colorize("#FFFF00", portableboxes.translate(newNameBox))
				.."\n"..core.colorize("#CCCCCC", portableboxes.translate("by: @1", ownername))
			end
			if not portableboxes.canIntect(pos, playername, newNameBox) then 
				return false 
			end
			
			local inv=meta:get_inventory()
			local items = {}
			local inbox = inv:get_list("inbox")
			if (type(inbox)=="table") then
				for i,v in pairs(inbox) do
					table.insert(items,v:to_table())
				end
			end
	
			local box = ItemStack(objname):to_table()
			box.meta={
				namebox = newNameBox,
				description = newDescription,
				items=minetest.serialize(items),
			}
			local pinv = player:get_inventory()
			if pinv:room_for_item("main",ItemStack(box)) then
				pinv:add_item("main", ItemStack(box))
				minetest.set_node(pos, {name = "air"})
				minetest.sound_play("default_dig_dig_immediate", {pos=pos, gain = 1.0, max_hear_distance = 5,})
			end
		end,
		on_receive_fields = function(pos, formname, fields, player)
			local playername = player:get_player_name()
			local isCanDig = portableboxes.canIntect(pos, playername)
			if isCanDig then
				if (fields.key_enter ~= nil or fields.btnSaveText ~= nil) and fields.namebox ~= nil and type(fields.namebox) == "string" then
					setMetaDefault(pos, player, fields.namebox:trim())
				end
			end
		end,
	}
	
	if def.can_tube_device ~= nil and type(def.can_tube_device) == "boolean" and def.can_tube_device == true then
		newDef.groups.tubedevice = 1
		newDef.groups.tubedevice_receiver = 1
		newDef.tube = {
			insert_object = function(pos, node, stack, direction)
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				local added = inv:add_item("inbox", stack)
				return added
			end,
	
			can_insert = function(pos, node, stack, direction)
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				return inv:room_for_item("inbox", stack)
			end,
	
			input_inventory = "inbox",
			connect_sides = {left = 1, right = 1, front = 1, back = 1, top = 1, bottom = 1}
		}
	end
	
	if def.description ~= nil and type(def.description) == "string" and def.description ~= "" then
		newDef.description = newDef.description 
		.."\n * "..def.description
	end
	
	if def.inventory_image ~= nil and type(def.inventory_image) == "string" and def.inventory_image ~= "" then
		newDef.inventory_image = def.inventory_image
	end
	
	minetest.register_node(objname, newDef)
	
	for i, newRecipe in ipairs(def.recipes) do
		minetest.register_craft({
			output = objname.." "..def.amount_per_recipe,
			recipe = newRecipe
		})
	end
	
	for i, shortname in ipairs(def.alias) do
		minetest.register_alias(
			shortname,
			objname
		)
	end
	
end
