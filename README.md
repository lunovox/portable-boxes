![screenshot]

# Portable Boxes
[[Minetest Mod]]

For Minetest 5.7.0 and later. 

### Usage

Adds portable boxes that can be carried in the player's inventory.

### Dependencies

**Mandatory:** 

* [default] : Minetest Game Included

**Optional:** 

* [intllib] : Facilitates the translation of several other mods into your native language, or other languages.
* [tradelands] : Protection of rent door (not yet implemented). 

### Recipe

![screenshot-recipe-woodbox]

* 6x Wood Plank
* 3x Metal Ingot

### License

* GNU AGPL: https://gitlab.com/lunovox/portable-boxes/-/raw/master/LICENSE

More details: 

 * English: https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
 * Portuguese: https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License


### Developers

* [AiTechEye] (by [Original Mod])
* [Lunovox Heavenfinder] (by [Forked Mod])

### Languages

* English (Default Language)
* Portuguse
* Brazilian Portuguse

If you want translate to other language, read the file [locale/READEME.md].

-------

[AiTechEye]:https://content.minetest.net/users/AiTechEye/
[CC BY-SA-4.0]:https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR
[default]:https://content.minetest.net/packages/Minetest/minetest_game/
[Forked Mod]:https://gitlab.com/lunovox/portable-boxes
[Minetest Mod]:https://www.minetest.net/
[intllib]:https://github.com/minetest-mods/intllib
[LGPL-2.1]:https://gitlab.com/lunovox/portable-boxes/-/raw/main/LICENSE
[locale/READEME.md]:https://gitlab.com/lunovox/portable-boxes/-/tree/main/locale
[Lunovox Heavenfinder]:https://libreplanet.org/wiki/User:Lunovox
[Original Mod]:https://content.minetest.net/packages/AiTechEye/hook/
[screenshot]:https://gitlab.com/lunovox/portable-boxes/-/raw/main/screenshot.png
[screenshot-recipe-woodbox]:https://gitlab.com/lunovox/portable-boxes/-/raw/main/screenshot-recipe-woodbox.png
[tradelands]:https://github.com/Lunovox/tradelands




