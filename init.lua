portableboxes={}

portableboxes.modname = minetest.get_current_modname()
portableboxes.modpath = minetest.get_modpath(portableboxes.modname)

dofile(portableboxes.modpath.."/translate.lua")
dofile(portableboxes.modpath.."/api.lua")
dofile(portableboxes.modpath.."/box_wood.lua")

minetest.log('action',"["..portableboxes.modname:upper().."] Loaded!")
