
portableboxes.register_box(
	"portableboxes:woodbox", 
	{
		title = portableboxes.translate("Portable Wood Box"),
		description = portableboxes.translate("Allows you to expand your inventory during your adventures."),
		label_default = portableboxes.translate("WOOD BOX"),
		slots = {
			width = 4, --max: 8
			height = 4 --max: 4
		},
		--inventory_image =  minetest.inventorycube("tex_woodbox_256x256.png"),
		tiles = {
			"tex_woodbox_256x256.png", --top
			"tex_woodbox_256x256.png", --bottom
			"tex_woodbox_256x256.png", --right
			"tex_woodbox_256x256.png", --left
			"tex_woodbox_256x256.png", --end
			"tex_woodbox_256x256.png", --from
		},
		box_format = {
			type = "fixed",
			fixed = { 
				{-.49,-.5,-.49,    .49,.49,.49}	
			}
		},
		sounds = {
			place = "default_place_node_hard",
			default = default.node_sound_wood_defaults(),
		},
		groups = {
			dig_immediate = 2, --Pode se retirado do chão com facilidade.
			falling_node = 1, --Despenca no chão se mal empilhado.
			flammable = 3, --Pode ser usado como combustível.
		},
		can_tube_device = true,
		recipes = {
			{--recipe1
				{"group:wood",				"group:wood",				"default:steel_ingot"},
				{"group:wood",				"default:steel_ingot", 	"group:wood"},
				{"default:steel_ingot",	"group:wood",				"group:wood"},
			},
			{--recipe2
				{"group:wood",					"group:wood",					"default:copper_ingot"},
				{"group:wood",					"default:copper_ingot", 	"group:wood"},
				{"default:copper_ingot",	"group:wood",					"group:wood"},
			},
			{--recipe3
				{"group:wood",					"group:wood",					"default:bronze_ingot"},
				{"group:wood",					"default:bronze_ingot", 	"group:wood"},
				{"default:bronze_ingot",	"group:wood",					"group:wood"},
			},
			{--recipe4
				{"group:wood",				"group:wood",				"default:silver_ingot"},
				{"group:wood",				"default:silver_ingot", 	"group:wood"},
				{"default:silver_ingot",	"group:wood",				"group:wood"},
			},
		},
		amount_per_recipe = 1,
		alias = {
			"woodbox", --/giveme woodbox
			"portablewoodbox", --/giveme portablewoodbox
			portableboxes.translate("woodbox"),
			portableboxes.translate("portablewoodbox"),
		},
	}
)